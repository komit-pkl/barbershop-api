<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_Model');
	}

	public function index_get()
	{
		$response = array(
			'message' => 'Welcome to AUTH API'
		);
		$this->response($response, 200);
	}

	public function login_post()
	{
		$post = $this->post();
		$this->form_validation->set_data($post);

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');

		if ($this->form_validation->run() == TRUE) {
			$userData = $this->Users_Model->login($post['username'], $post['password']);
			if ($userData) {
				$response = array(
					'message' => 'Login Success',
					'success' => TRUE
				);

				$token['id'] = $userData->id;
				$token['username'] = $userData->username;	
				$token['logged'] = TRUE;
				$token['iat'] = time();
				$token['exp'] = time() + 86400;
				$jwt_token = JWT::encode($token,secretKey());
				$jwt = array(
					'token' => $jwt_token,
				);

				$response = array_merge($response,$jwt);

				$data = array(
					'last_login' => date('Y-m-d H:i:s'),
					'token' => $jwt_token
				);

				$this->Users_Model->update($data, $userData->id);
			}else{
				$response = array(
					'anu' => $userData,
					'message' => 'Login Failed',
					'success' => FALSE
				);
			}
		} else {
			$response = array(
				'errors' => $this->form_validation->error_array(),
				'success' => FALSE
			);
		}

		$this->response($response, 200);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/api/Auth.php */
