<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_Model');
	}

	public function index_post()
	{
		$post = $this->post();
		$this->form_validation->set_data($post);

		$this->form_validation->set_rules('first_name', 'Fisrt Name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('level', 'Level', 'trim|required');

		if ($this->form_validation->run() == TRUE) {
			
			$data = array(
				'first_name' => $post['first_name'],
				'last_name' => $post['last_name'],
				'username' => $post['username'],
				'password' => $post['password'],
				'level' => $post['level']
			);

			$insert = $this->Users_Model->insert($data);

			if ($insert) {
				$response = array(
					'message' => "User successfully created",
					'success' => TRUE
				);
			}else{
				$response = array(
					'message' => "Sorry, can't create user",
					'success' => FALSE
				);
			}

		} else {
			$response = array(
				'errors' => $this->form_validation->error_array(),
				'success' => FALSE
			);
		}

		$this->response($response, 200);

	}

}

/* End of file Users.php */
/* Location: ./application/controllers/api/Users.php */
