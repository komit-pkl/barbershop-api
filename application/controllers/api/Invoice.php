<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Invoice extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Barber_Model');
		$this->load->database();
	}

	public function index_get($id)
	{
		
		$getInvoice = $this->Barber_Model->getInvoice($id)->result_array();
		$invoice = array();
		foreach ($getInvoice as $key => $row) {
			$invoice[]['invoice'] = array(
				'ReservationID' => $row['id'],
				'Full_Name' => $row['full_name'],
				'Date' => $row['created_date']
			);
			$id = $row['id'];
		}
		$service = array();
		$getservice = $this->Barber_Model->getReservationDetailWhere($id)->result();
		foreach ($getservice as $row) {
			$service[] = array(
				'Service' => $row->service_id,
				'Name' => $row->name,
				'Price' => $row->price
			);
		}
		$invoice[$key]['Service'] = $service;
		$getpayment = $this->Barber_Model->getReservationPaymentWhere($id)->result();
		foreach ($getpayment as $row) {
			$payment = array(
				'Bill_Amount' => $row->bill_amount,
				'Cash_Amount' => $row->cash_amount,
				'Change_Amount' => $row->change_amount
			);
		}
		$invoice[$key]['Payment'] = $payment;
			
		// }
		
		$data = array(
			'data' => $invoice,
			'success' => true
		);

		$this->response($data,200);
	}

}

/* End of file Invoice.php */
/* Location: ./application/controllers/api/Invoice.php */