<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';


class Reservation_Payment extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Barber_Model');
		$this->load->database();
	}
	
	public function index_get()
	{
		$getReservationDetail = $this->Barber_Model->getReservationPayment()->result();
		$reservation = array();
		foreach ($getReservationDetail as $key => $row) {
			$reservation[]['Payment'] = array(
				'id' => $row->id,
				'Reservation ID' => $row->reservation_id,
				'Cash Amount' => $row->cash_amount,
				'Bill Amount' => $row->bill_amount,
				'Change Amount' => $row->change_amount
			);
		}

		$data = array(
			'data' => $reservation,
			'success' => true
		);

		$this->response($data,200);
	}

}

/* End of file Reservation_Detail.php */
/* Location: ./application/controllers/api/Reservation_Detail.php */