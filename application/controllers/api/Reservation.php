<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Reservation extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct();
		$this->load->model('Barber_Model');
	}

	public function index_get()
	{
		$getReservation = $this->Barber_Model->getReservation()->result();
		$reservation = array();
		foreach ($getReservation as $key => $row) {
			$reservation[]['Reservation'] = array(
						'id' => $row->id,
						'Full Name' => $row->full_name,
						'status' => $row->status,
						'Note' => $row->note,
						'Phone' => $row->phone,
						'Created Date' => $row->created_date
			);
		}
			$data = array(
				'data' => $reservation,
				'status' => true
			);

			$this->response($data,200);
		
	}
	
	public function index_post()
	{
		$total = 0;
		$data = array(
			'id'	=> $this->post('id'),
			'full_name'	=> $this->post('full_name'),
			'status'	=> 'Received',
			'note'		=> $this->post('note'),
			'phone'		=> $this->post('phone'),
			'created_date'	=> date('Y-m-d')
		);
		if ($this->Barber_Model->addReservation('reservation', $data)) {
			$service_id = $this->post('service_id');
			for ($i=0; $i < count($service_id); $i++) { 
				$data = array(
					'reservation_id'	=> $this->post('id'),
					'service_id'		=> $service_id[$i],
					'created_date'		=> date('Y-m-d')
				);
				$detail = $this->Barber_Model->addReservation('reservation_detail', $data);
			}
			if ($detail) {
				$service_id = $this->post('service_id');
				for ($a=0; $a < count($service_id); $a++) { 
					$get_Price = $this->Barber_Model->getPrice($service_id[$a])->result();
					$total += $get_Price[0]->price;
				}	
				$data = array(
					'reservation_id'	=> $this->post('id'),
					'bill_amount'		=> $total,
					'cash_amount'		=> NULL,
					'change_amount'		=> NULL,
					'created_date'		=> date('Y-m-d')

				);
				if ($this->Barber_Model->addReservation('reservation_payment', $data)) {
					$this->response(array('success' => TRUE, 'message' => 'Data successfully added'), 200);
				} else {
					$this->response(array('success' => false, 'message' => 'null'), 200);
				}
			}
		}

	}
	public function index_put()
	{
	    $id = $this->put('id');
	    $getpayment = $this->Barber_Model->getReservationPaymentWhere($id)->result();
	    foreach ($getpayment as $row) {
		$data = array(
			'id'                => $this->put('id'),
			'cash_amount'		=> $this->put('cash_amount'),
			'change_amount'		=> $this->put('cash_amount') - $row->bill_amount,
		);
		}
		$update = $this->Barber_Model->updateReservationPayment($id,$data);
		if ($update) {
			$this->response(array('success' => TRUE, 'message' => 'Data successfully added'), 200);
		}else{
			$this->response(array('success' => FALSE, 'message' => 'null'), 200);
		}
	}
	public function index_delete($id)
	{
		$delete = $this->Barber_Model->deleteService($id);
		if($delete){
			$this->response(array('message' => 'success','status' => true),200);
		}else{
			$this->response(array('message' => 'fail','status' => fail),200);
		}
	}

}

/* End of file Reservation.php */
/* Location: ./application/controllers/api/Reservation.php */