<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {

	public function index_get()
	{
		$result = array(
			'message' => 'Welcome to Barbershop API'
		);
		$this->response($result);
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/api/Api.php */
