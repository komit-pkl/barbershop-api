<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Services extends REST_Controller{

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Barber_Model');
		$this->load->database();
	}

	public function index_get()
	{
		$getServices = $this->Barber_Model->getServices()->result();
		$services = array();
		foreach ($getServices as $key => $row) {
			$services[]['Service'] = array(
				'id' => $row->id,
				'Name' => $row->name,
				'Price' => $row->price
			);
		}

			$data = array(
				'data' => $services,
				'success' => true
			);

		$this->response($data,200);	
	}
}

/* End of file Services.php */
/* Location: ./application/controllers/api/Services.php */