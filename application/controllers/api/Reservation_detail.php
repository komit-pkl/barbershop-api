<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Reservation_detail extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Barber_Model');
		$this->load->database();
	}

	public function index_get()
	{
		$getReservationDetail = $this->Barber_Model->getReservationDetail()->result();
		$reservation = array();
		foreach ($getReservationDetail as $key => $row) {
			$reservation[]['Reservation Detail'] = array(
				'id' => $row->reservation_id,
				'Service ID' => $row->service_id
			);
		}

		$data = array(
			'data' => $reservation,
			'success' => true
		);

		$this->response($data,200);
	}

}

/* End of file Reservation_detail.php */
/* Location: ./application/controllers/api/Reservation_detail.php */