<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barber_C extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Barber_Model');
		$this->load->database();
	}
	public function index()
	{
		$data['list'] = $this->Barber_Model->getWaitingList();
		$this->load->view('index', $data, FALSE);
	}

}

/* End of file Barebr_C.php */
/* Location: ./application/controllers/Barebr_C.php */