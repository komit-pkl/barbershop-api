<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barber_Model extends CI_Model {

	public function getPrice($id = NULL)
	{
		$this->db->select('price');
		$this->db->from('service');
		$this->db->where('id', $id);
		return $this->db->get();		
	}

	public function addReservation($table,$data)
	{
		$query = $this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function getReservation()
	{
		return $this->db->get('reservation');
	}

	public function getReservationWhere($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('reservation');
	}

	public function createReservation($data)
	{
		return $this->db->insert('reservation',$data);
	}

	public function updateReservation($data)
	{
		$this->db->where('id',$id);
		return $this->db->update('reservation',$data);
	}

	public function deleteReservation($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('reservation');
	}

	//function reservation detail
	public function getReservationDetail()
	{
		return $this->db->get('reservation_detail');
	}

	public function getReservationDetailWhere($id)
	{
		$this->db->select('a.*,b.*')
		->from('service a')
		->join('reservation_detail b','b.service_id = a.id','left')
		->where('reservation_id',$id);
		return $this->db->get();
	}

	public function createReservationDetail($data)
	{
		return $this->db->insert('reservation_detail',$data);
	}

	public function deleteReservationDetail($id)
	{
		$this->db->where('reservation_id',$id);
		return $this->db->delete('reservation_detail');
	}

	//function Reservation Payment
	public function getReservationPayment()
	{
		return $this->db->get('reservation_payment');
	}

	public function getReservationPaymentWhere($id)
	{
		$this->db->where('reservation_id',$id);
		return $this->db->get('reservation_payment');
	}
	public function updateReservationPayment($id, $data)
	{
		$this->db->where('reservation_id', $id);
		return $this->db->update('reservation_payment', $data);
	}

	//function Services
	public function getServices()
	{
		return $this->db->get('service');
	}

	public function getServiceWhere($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('service');
	}

	//function Invoice
	public function getInvoice($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('reservation');
	}
	
}

/* End of file Barber_Model.php */
/* Location: ./application/models/Barber_Model.php */