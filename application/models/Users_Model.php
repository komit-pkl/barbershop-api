<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

	private $table = "users";

	function insert($data)
	{
		$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		$data['created_date'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table, $data);
		
		if ($this->db->affected_rows() === 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function update($data, $id)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		if ($update) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function login($username, $password)
	{
		$user = $this->isValidUser($username);
		if ($user) {
			if (password_verify($password, $user->password)) {
				return $user;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	function isValidUser($username)
	{
		$this->db->where('username', $username);
		$data = $this->db->get($this->table)->row();
		if ($data) {
			return $data;
		}else{
			return FALSE;
		}
	}

}

/* End of file Users_Model.php */
/* Location: ./application/models/Users_Model.php */
