<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends REST_Controller {

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('Authorization');		

		if ($token) {

			$token = explode("Bearer ", $token);

			$jwt = JWT::decode($token[1],secretKey(),true);

			if (@$jwt->logged AND time() < @$jwt->exp) {
				$this->jwtData = $jwt;
			}else{
				$response = array(
					'message' => 'Failed to authenticate token',
					'success' => FALSE
				);
				$this->response($response, 400);
			}

		}else{
			$response = array(
				'message' => 'Failed to authenticate token',
				'success' => FALSE
			);
			$this->response($response, 400);
		}

	}

}

/* End of file API_Controller.php */
/* Location: ./application/core/API_Controller.php */
