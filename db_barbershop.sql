-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 16 Bulan Mei 2018 pada 06.57
-- Versi server: 5.5.56-MariaDB
-- Versi PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barbershop_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `status` enum('Received','Processing','Done') NOT NULL,
  `note` varchar(125) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation`
--

INSERT INTO `reservation` (`id`, `full_name`, `status`, `note`, `phone`, `created_date`) VALUES
(1, 'Axel Setiawan', 'Received', 'adassad', '087785037333', '2018-05-09'),
(2, 'Ichsandit', 'Received', 'Warnain Rambut', '087785037333', '2018-05-31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation_detail`
--

CREATE TABLE `reservation_detail` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation_detail`
--

INSERT INTO `reservation_detail` (`id`, `reservation_id`, `service_id`, `created_date`) VALUES
(1, 1, 1, '0000-00-00'),
(3, 2, 1, '0000-00-00'),
(5, 1, 3, '0000-00-00'),
(6, 2, 5, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation_payment`
--

CREATE TABLE `reservation_payment` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `cash_amount` decimal(10,2) NOT NULL,
  `bill_amount` decimal(10,2) NOT NULL,
  `change_amount` decimal(10,2) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation_payment`
--

INSERT INTO `reservation_payment` (`id`, `reservation_id`, `cash_amount`, `bill_amount`, `change_amount`, `created_date`) VALUES
(1, 1, '50000.00', '25000.00', '25000.00', '0000-00-00'),
(2, 2, '100000.00', '50000.00', '50000.00', '2018-05-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`id`, `name`, `price`, `created_date`) VALUES
(1, 'Haircut ', 25000.00, '2018-05-15'),
(2, 'Hair Style', 30000.00, '2018-05-15'),
(3, 'Massages', 20000.00, '2018-05-15'),
(4, 'Facials / Face Massages', 25000.00, '2018-05-15'),
(5, 'Appearance Service', 20000.00, '2018-05-15'),
(6, 'Beard Service', 15000.00, '2018-05-15'),
(7, 'Shoeshines', 10000.00, '2018-05-15'),
(8, 'Moustache Trim', 15000.00, '2018-05-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `created_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `level`, `img`, `created_date`) VALUES
(1, 'dea aulia', 'salsabila', 'deaaulia31', '081295423446d', 'admin', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reservation_detail`
--
ALTER TABLE `reservation_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Indeks untuk tabel `reservation_payment`
--
ALTER TABLE `reservation_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Indeks untuk tabel `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `reservation_detail`
--
ALTER TABLE `reservation_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `reservation_payment`
--
ALTER TABLE `reservation_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `reservation_detail`
--
ALTER TABLE `reservation_detail`
  ADD CONSTRAINT `reservation_detail_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservation_detail_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `reservation_payment`
--
ALTER TABLE `reservation_payment`
  ADD CONSTRAINT `reservation_payment_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
